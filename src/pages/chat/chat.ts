import {Component, ElementRef, ViewChild} from '@angular/core';
import {Content, IonicPage, NavController, NavParams, ActionSheetController} from 'ionic-angular';
import * as firebase from 'firebase/app';
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth";
import * as moment from "moment";
import {Observable} from "rxjs/Observable";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { StorageProvider } from '../../providers/storage';
/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-chat',
    templateUrl: 'chat.html',
})
export class ChatPage {

    public restaurant;
    public user;
    public firebaseuser;
    public chatKey;
    public chatRoom;
    public messages: any = [];
    public messageRef: Observable<{}>;
    private mutationObserver: MutationObserver;
    showEmojiPicker = false;
    @ViewChild(Content) content: Content;
    @ViewChild('chat_input') messageInput: ElementRef;
    @ViewChild('msglist',{read: ElementRef}) msglist: ElementRef;
    newMessage = '';
    editorMsg = '';
    storageRef:any;
    constructor(public actionSheetCtrl: ActionSheetController,
      public camera: Camera,public navCtrl: NavController, 
      public navParams: NavParams, public afDb: AngularFireDatabase, 
      public afAuth: AngularFireAuth,
      public storageprovide:StorageProvider) {
      this.storageRef = firebase.storage().ref('/chatimages/');
      this.restaurant = this.navParams.get("restaurant");
      this.user = this.navParams.get("user");
      console.log('getting user from nav param',this.user);
    }

    ionViewDidLoad() {
    //   this.mutationObserver = new MutationObserver((mutations) => {
    //     this.content.scrollToBottom();
    // });

    // this.mutationObserver.observe(this.msglist.nativeElement, {
    //     childList: true
    // });
         const instance=this; 
        this.afAuth.user.subscribe(res => {
       if(res!==null){
        instance.firebaseuser = res.toJSON();
        console.log(this.user,this.restaurant);
        if (this.user && this.restaurant) {
          this.chatKey = this.restaurant.owner_id + ":" + this.firebaseuser.uid;
          console.log(this.user,this.restaurant);
          this.getCreateChatRoom();
      }
       }
         
        })
    

    }

    getCreateChatRoom() {

     
        console.log("ChatKey", this.chatKey);
        console.log("User display name =", this.user.displayName);
        console.log("User display name =", this.user.displayName);
        if (this.user.displayName == null) {
            this.user.displayName = "Unknown User";
        }
        let chatCreateData = {
            vendor_id: this.restaurant.owner_id,
            user_id: this.firebaseuser.uid,
            user_name: this.user.displayName,
            vendor_title: this.restaurant.title,
            hasUnreadMsg:false,
            last_message:''
        };
        // let chatRooms = this.afDb.object("/chatrooms/" + this.chatKey);
        console.log("creating chatroom with key",this.chatKey);
         console.log("data from chat creating chatroom",chatCreateData);
        this.afDb.object("/chatrooms/" + this.chatKey).valueChanges().subscribe(value => {
            console.log(value);
            this.chatRoom = value;
            if (!value || value == null) {
                this.afDb.object("/chatrooms/" + this.chatKey).set(chatCreateData).then(res => {
                    console.log(res);
                }).catch(e => {
                    console.error(e);
                });
            }
        });
       
        this.afAuth.user.subscribe(res => {
          this.user = res.toJSON();
          
          firebase.database().ref("/chatmessages/" + this.chatKey).on('child_added', (snapshot)=> {
            console.log("child added user")
            var msgData = snapshot.toJSON();
            Object.assign(msgData,{key:snapshot.key});
            this.messages.push(msgData);
         });
         firebase.database().ref("/chatmessages/" + this.chatKey).on('child_changed', (snapshot)=> {
          console.log("child updated user")
          var msgData = snapshot.toJSON();
          this.messages.map(msg=>{
            if(msg.key===snapshot.key) msg.msg_readed=true;
            return msg;
          })
     
       });
        });
        // let messages = this.afDb.list("/chatmessages/" + this.chatKey).valueChanges();
        // console.log(messages);
        // console.log(this.messageRef);
        // messages.subscribe(res => {
        //     console.log(res);
        // });

        // firebase.database().ref("/chatmessages/" + this.chatKey).orderByChild("timestamp").on("child_added", res => {
        //     this.messages.push(res.toJSON());
        // });
        // this.messageRef = this.afDb.list("/chatmessages/" + this.chatKey);
        //
        // this.messageRef.valueChanges().subscribe(res => {
        //     console.log(res);
        // });

        // console.log(chatRooms);

        // this.afDb.object("/chatrooms/"+this.chatKey).valueChanges()
        // firebase.database().refFromURL("/chatrooms/"+this.chatKey).child("messages").on("v")


    }

    sendMessage(message) {
        var  timestamp = moment().unix();
        this.chatKey = this.restaurant.owner_id + ":" + this.firebaseuser.uid;
        let data = {
            "sender_id": this.firebaseuser.uid,
            "receiver_id": this.restaurant.owner_id,
            "message": message,
            "timestamp": timestamp,
            "msg_readed":false
        };
         console.log("chat key when sending new message",this.chatKey);
        this.afDb.list("/chatmessages/" + this.chatKey).push(data).then(res => {
            console.log(res);
            this.editorMsg = '';
        });
        firebase.database().ref(`/chatrooms/${this.chatKey}`).update({"hasUnreadMsg":true,"last_message":message,"last_msg_timestamp":timestamp},(err)=>{
          if(err){
            console.error("err in err => ",err)
          } else {
            console.log("no error in err => ",this)
          }
        })
    }

    switchEmojiPicker() {
        this.showEmojiPicker = !this.showEmojiPicker;
        if (!this.showEmojiPicker) {
            this.focus();
        } else {
            this.setTextareaScroll();
        }
        this.content.resize();
        this.scrollToBottom();
    }

    scrollToBottom() {
        // setTimeout(() => {
        //     if (this.content.scrollToBottom) {
        //         this.content.scrollToBottom();
        //     }
        // }, 400)
        this.content.scrollToBottom();
    }

    private focus() {
        if (this.messageInput && this.messageInput.nativeElement) {
            this.messageInput.nativeElement.focus();
        }
    }

    private setTextareaScroll() {
        const textarea = this.messageInput.nativeElement;
        textarea.scrollTop = textarea.scrollHeight;
    }

    onFocus() {
      this.updateMessageStatus();
        this.showEmojiPicker = false;
        this.content.resize();
        this.scrollToBottom();
    }
    updateMessageStatus(){
      this.chatKey = this.restaurant.vendor_id + ":" + this.restaurant.user_id;
      
     console.log("focusing....")
      this.afAuth.user.subscribe(res => {
        this.user = res.toJSON();
        this.messages.forEach(msg => {
          console.log("msg key",msg.key);
          if(!msg.msg_readed){
            firebase.database().ref("/chatmessages/" + this.chatKey+"/"+msg.key)
            .update({"msg_readed":true},(err)=>{
              if(err){
                console.error("err in err => ",err)
              } else {
                console.log("no error in err => ",this)
              }
            })
          }
        
        });
      
      
    
      });
    }
    openGallery() {
      let actionSheet = this.actionSheetCtrl.create({
        enableBackdropDismiss: true,
        buttons: [
          {
            text: 'Take a picture',
            icon: 'camera',
            handler: () => {
              this.takePicturefromCamera();
            }
          }, {
            text: 'From gallery',
            icon: 'images',
            handler: () => {
              this.takePicture();
            }
          }
        ]
      });
      actionSheet.present();
    }
  
    takePicture() {
  
      const options: CameraOptions = {
        allowEdit: true,
        saveToPhotoAlbum: true,
        targetWidth: 720,
        targetHeight: 720,
        mediaType: this.camera.MediaType.PICTURE,
  
        sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.PNG,
        correctOrientation: true
      };
      this.camera.getPicture(options).then((imagePath) => {
        if(imagePath != ''){
          this.uploadPhoto(imagePath);
        }
      }).catch((err)=>{
  
      })
    }
  
    takePicturefromCamera() {
  
      const options: CameraOptions = {
        allowEdit: true,
        saveToPhotoAlbum: true,
        targetWidth: 720,
        targetHeight: 720,
        cameraDirection: this.camera.Direction.BACK,
        sourceType: this.camera.PictureSourceType.CAMERA,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.PNG,
        correctOrientation: true
      };
      this.camera.getPicture(options).then((imagePath) => {
        if(imagePath != ''){
          this.uploadPhoto(imagePath);
        }
      }).catch((err)=>{
        console.log(err)
      })
  
  
    }
  
    uploadPhoto(imagePath) {
  
      this.storageRef.child(this.generateUUID() + ".png")
        .putString(imagePath, 'base64', { contentType: 'image/png' })
        .then((savedPicture) => {
          console.log(savedPicture.downloadURL);
          var timestamp=moment().unix()
          let data = {
            "sender_id": this.firebaseuser.uid,
            "receiver_id": this.restaurant.owner_id,
            "message": '',
            "message_type": 'image',
            "firebase_url": savedPicture.downloadURL,
            "image": "/chatimages/" + this.generateUUID() + ".png",
            "img": this.generateUUID() + ".png",
            "timestamp": timestamp
          };
          this.afDb.list("/chatmessages/" + this.chatKey).push(data).then(res => {
            this.editorMsg = '';
            firebase.database().ref(`/chatrooms/${this.chatKey}`).update({"hasUnreadMsg":true,"last_message":'User sent attachment',"last_msg_timestamp":timestamp},(err)=>{
              if(err){
                console.error("err in err => ",err)
              } else {
                console.log("no error in err => ",this)
              }
            })
          });
          
        }).catch((err) => {
          console.log(JSON.stringify(err))
        });
    }
  
    private generateUUID(): any {
      var d = new Date().getTime();
      var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
      });
      return uuid;
    }

}

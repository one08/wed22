import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase/app';
import {AngularFireDatabase} from "angularfire2/database";
import {AngularFireAuth} from "angularfire2/auth"
/**
 * Generated class for the FindbycategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-findbycategory',
  templateUrl: 'findbycategory.html',
})
export class FindbycategoryPage {
  
  categorList:any;
  constructor(public navCtrl: NavController,
     public navParams: NavParams,public afDb: AngularFireDatabase, 
     public afAuth: AngularFireAuth) {
      this.afAuth.user.subscribe(res => {
        this.afDb.object("/category").valueChanges().subscribe(value => {
          this.categorList=[];
          for(var key in value){
            var cat = value[key];
            this.categorList.push(cat);
          }
        console.log(this.categorList);
    });
  
  });
  }

  ionViewDidLoad() {
  
  }
}
